package orderhelper;
/**
 * A product is a part of an order ordered by a customer, for example: a salad or a soda.
 * @author Astonaut
 *
 */

public class Product { 				//The product
	private String name;			//Name of the product
	private int quantity;			//Amount ordered
	private String comment;		//Comment from customer

	public Product(String name, int quantity, String comment) {
		this.name = name;
		this.quantity = quantity;
		this.comment = comment;
	}

	/**
	 * Get the product name. 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the quantity of the product. 
	 * @return
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Get the product description.
	 * @return description
	 */
	public String getComment() {
		return comment;
	}

}
