package orderhelper;

import java.util.concurrent.TimeUnit;

/**
 * A customer orders an order consisting of one or multiple products.
 * @author Astonaut
 *
 */
public class Order {
	
	private Product[] products; //The products ordered
	private int orderID;		//Unique ID to each order even if same customer
	private Customer customer;	//The customer ordering
	private int price;			//The price of the order
	private OrderStatus status;	//The status of the order
	private long time;			//The time the order was created. 

	public Order(Product[] products, Customer customer, int price) {
		this.products = products;
		this.customer = customer;
		this.price = price;
		this.orderID = OrderID.getID();
		this.status = OrderStatus.V�NTAR;
		time = System.nanoTime();
	}
	

	/**
	 * Get the products of an order.
	 * @return array of products. 
	 */
	public Product[] getProducts() {
		return products;
	}
	
	/**
	 * Get the customer of an order.
	 * @return customer of the order.
	 */
	public Customer getCustomer() {
		return customer;
	}
	
	/**
	 * Get the price of an order.
	 * @return price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * Get the unique order ID.
	 * @return orderID
	 */
	public int getOrderID() {
		return orderID;
	}
	
	/**
	 * Get the status of the order.
	 * @return
	 */
	public OrderStatus getStatus() {
		return status;
	}
	
	
	/**
	 * Set the status of the order.
	 * @param newStatus
	 */
	public void setStatus(OrderStatus newStatus) {
		status = newStatus;
	}
	
	/**
	 * Get the elapsed hours. 
	 * @return time
	 */
	public long getElapsedHours() {
		long elapsed = System.nanoTime() - time;
		long hours = TimeUnit.HOURS.convert(elapsed, TimeUnit.NANOSECONDS);
		
		return Math.round(hours);
	}
	
	/**
	 * Get the elapsed minutes.
	 * @return minutes.
	 */
	public long getElapsedMinutes() {
		long elapsed = System.nanoTime() - time;
		long minutes = TimeUnit.MINUTES.convert(elapsed, TimeUnit.NANOSECONDS);
		
		return Math.round(minutes);
	}
	
}
