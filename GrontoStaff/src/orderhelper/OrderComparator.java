package orderhelper;

import java.util.Comparator;

public class OrderComparator<T> implements Comparator<T> {

	@Override
	public int compare(T t1, T t2) {
		Order o1 = (Order) t1;
		Order o2 = (Order) t2;
		if (o1.getStatus() == o2.getStatus()) {
			if (o1.getElapsedHours() == o2.getElapsedHours()) {
				return Long.compare(o1.getElapsedMinutes(), o2.getElapsedMinutes());
			} else {
				return Long.compare(o1.getElapsedHours(), o2.getElapsedHours());
			}
		} else if (o1.getStatus() == OrderStatus.SEN) {
			switch (o2.getStatus()) {
			case LEVERERAD:
				return -1;
			case KLAR:
				return -1;
			case V�NTAR:
				return -1;
			default:
				break;
			}
		} else if (o1.getStatus() == OrderStatus.V�NTAR) {
			switch (o2.getStatus()) {
			case LEVERERAD:
				return -1;
			case KLAR:
				return -1;
			case SEN:
				return 1;
			default:
				break;
			}
		} else if (o1.getStatus() == OrderStatus.KLAR) {
			switch (o2.getStatus()) {
			case LEVERERAD:
				return -1;
			case SEN:
				return 1;
			case V�NTAR:
				return 1;
			default:
				break;
			}
		} else if (o1.getStatus() == OrderStatus.LEVERERAD) {
			switch (o2.getStatus()) {
			case SEN:
				return 1;
			case KLAR:
				return 1;
			case V�NTAR:
				return 1;
			default:
				break;
			}
		}
		return 0;
	}

}
