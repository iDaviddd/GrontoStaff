package orderhelper;

public class Customer {			//A customer who has ordered
	private String name;		//Following variables is information to the customer
	private String address;		
	private String zipCode;
	private String cellPhone;
	private String email;
	private String city;
	public Customer(String name, String address, String zipCode, String cellphone, String email, String city) {
		this.name = name;
		this.address = address;
		this.zipCode = zipCode;
		this.cellPhone = cellphone;
		this.email = email;
		this.city = city;
	}

	/**
	 * Get the first and last name of a customer. 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the address of a customer in String format.
	 * @return Address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Get the city of a customer in String format. 
	 * @return city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Get the zip code of a customer in String format. 
	 * @return zip code
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * Get the phone number of a customer in String format. 
	 * @return phone number
	 */
	public String getCellPhone() {
		return cellPhone;
	}

	/**
	 * Get the email of a customer in String format. 
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

}
