package orderhelper;

import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;

import javafx.stage.Stage;
import order.OrderWindow;

public class OrderHandler {

	public static HashSet<Order> allOrders = new HashSet<Order>();
	private static OrderWindow ow;
	public static Stage orderstage = new Stage();

	public static void initialize() {
		ow = new OrderWindow();

		startTimer();

		// Temporary part
		Product[] products = new Product[1];
		Product product = new Product("Caesar", 2, "Utan sallad.");
		products[0] = product;
		Customer customer = new Customer("David Albertsson", "Fajansv�gen 92", "238 40", "0733555210",
				"david.albertsson@hotmail.com", "Malm�");
		Order order = new Order(products, customer, 118);
		addOrder(order);

		Product[] products2 = new Product[13];
		Product product2 = new Product("Grabbarna Special", 11, "Special");
		Product astProduct0 = new Product("Pr�bbes bl�a 5,0%", 6, "Ljumna");
		Product astProduct1 = new Product("Caesarsallad", 2, "Vaskade");
		Product astProduct2 = new Product("Smoothie - Advokadof�ra", 1, "Vaskade");
		Product astProduct3 = new Product("Smoothie - Himmelsbl�", 1, "Bongade");
		Product astProduct4 = new Product("Loka - Citron", 1, "");
		Product astProduct5 = new Product("Pepsi", 2 ,"");
		Product astProduct6 = new Product("Asiatisk R�ksallad", 1, "");
		Product astProduct7 = new Product("Laxsallad", 1, "");
		Product astProduct8 = new Product("Sallad + Glasnudlar" + 
				"Handskalade r�kor fr�n Sm�gen, Avocado, Cashewn�tter, Chevreost, Fetaost, F�rsk koriander, Gurka, Jalapeno, Ceasardressing, Honungsdijon, Norsk fjordlax, Handskalade r�kor fr�n Sm�gen, Krutonger, Majs, Paprika, Rivna mor�tter, Ruccola, Sojab�nor, Tomat, �gg",1, "");
		Product astProduct9 = new Product("Vitamin Well - Antioxidant: Persika",1,"");
		Product astProduct10 = new Product("Loka Naturell",1,"");
		Product astProduct11 = new Product("Pepsi Max", 3, "Kokad");
		products2[0] = product2;
		products2[1] = astProduct0;
		products2[2] = astProduct1;
		products2[3] = astProduct2;
		products2[4] = astProduct3;
		products2[5] = astProduct4;
		products2[6] = astProduct5;
		products2[7] = astProduct6;
		products2[8] = astProduct7;
		products2[9] = astProduct8;
		products2[10] = astProduct9;
		products2[11] = astProduct10;
		products2[12] = astProduct11;
		Customer customer2 = new Customer("Aston �kerman", "Arkivgatan 1B", "587 58", "0708 587 433",
				"aston.akerman@gmail.com", "Lund");
		Order order2 = new Order(products2, customer2, 1243);
		addOrder(order2);

		Product[] products3 = new Product[1];
		Product product3 = new Product("Grabbarna Special", 1, "Special");
		products3[0] = product3;
		Customer customer3 = new Customer("Anthony Bui", "Helenetorpsgatan50", "214 58", "0708 587 433", "ant.bui@dickmail.xyz",
				"Malm�");
		Order order3 = new Order(products3, customer3, 55);
		order3.setStatus(OrderStatus.LEVERERAD);
		addOrder(order3);

		Product[] products4 = new Product[1];
		Product product4 = new Product("Grabbarna Special", 1, "Special");
		products4[0] = product4;
		Customer customer4 = new Customer("Viktor Karlsson", "Juryv�gen 36", "226 57", "0700 000 000",
				"vihoa421@gmail.com", "Lund");
		Order order4 = new Order(products4, customer4, 55);
		order4.setStatus(OrderStatus.KLAR);
		addOrder(order4);

		Product[] products5 = new Product[1];
		Product product5 = new Product("Grabbarna Special", 1, "Special");
		products5[0] = product5;
		Customer customer5 = new Customer("Damil Sabotic", "�stra Hindbyv�gen 33", "213 74", "6969 420 420",
				"da4210sa-s@student.lu.se", "Malm�");
		Order order5 = new Order(products5, customer5, 55);
		order5.setStatus(OrderStatus.SEN);
		addOrder(order5);

		Product[] products6 = new Product[1];
		Product product6 = new Product("Grabbarna Special", 1, "Special");
		products6[0] = product6;
		Customer customer6 = new Customer("Oscar Wiklund", "Fagottgr�nden 17A", "224 68", "6969 420 420",
				"mat15-s@student.lu.se", "Lund");
		Order order6 = new Order(products6, customer6, 55);
		addOrder(order6);

		Product[] products7 = new Product[1];
		Product product7 = new Product("Grabbarna Special", 1, "Special");
		products7[0] = product7;
		Customer customer7 = new Customer("Jakob Sebastian Bengtsson", "Vinstorpsv�gen 18", "234 31", "040-41 66 43",
				"bassebengan@dickmail.xyz", "Lomma");
		Order order7 = new Order(products7, customer7, 55);
		addOrder(order7);

		Product[] products8 = new Product[1];
		Product product8 = new Product("Grabbarna Special", 1, "Special");
		products8[0] = product8;
		Customer customer8 = new Customer("Oscar Wiklund", "Fagottgr�nden 17A", "224 68", "0700 000 000",
				"oggewikkan@dickmail.xyz", "Lund");
		Order order8 = new Order(products8, customer8, 55);
		addOrder(order8);

		Product[] products9 = new Product[1];
		Product product9 = new Product("Grabbarna Special", 1, "Special");
		products9[0] = product9;
		Customer customer9 = new Customer("Aston �kerman", "Arkivgatan 1B", "587 58", "0708 587 433",
				"aston.akerman@dickmail.xyz", "Lund");
		Order order9 = new Order(products9, customer9, 55);
		addOrder(order9);

		Product[] products10 = new Product[1];
		Product product10 = new Product("Grabbarna Special", 1, "Special");
		products10[0] = product10;
		Customer customer10 = new Customer("Aston �kerman", "Arkivgatan 1B", "587 58", "0708 587 433",
				"aston.akerman@dickmail.xyz", "Lund");
		Order order10 = new Order(products10, customer10, 55);
		addOrder(order10);
		
		ow.sortList();
	}

	private static void startTimer() {
		final Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				if (ow.getIsOpen()) {
					ow.sortList();
				}
			}
		}, 500, 30000);
	}

	public static void newOrderWindow() {
		ow = new OrderWindow();
		ow.sortList();
	}

	/**
	 * 
	 * @param orderID
	 *            searches for order with orderID
	 * @return null if there are no orders or no with matching ID. Otherwise return
	 *         order with id orderID.
	 */
	public static Order getOrder(int orderID) {
		Order order = null;
		for (Order o : allOrders) {
			if (o.getOrderID() == orderID) {
				order = o;
			}
		}
		return order;
	}

	/**
	 * Add an order to the application.
	 * 
	 * @param order
	 *            to be added
	 */
	public static void addOrder(Order order) {
		allOrders.add(order);
		ow.addOrder(order);
	}

	/**
	 * Removes an order from the application.
	 * 
	 * @param order
	 *            to be removed
	 */
	public static void removeOrder(Order order) {
		allOrders.remove(order);
	}

	/**
	 * Check whether order exist.
	 * 
	 * @param order
	 *            to be checked
	 * @return true if exist, otherwise false
	 */
	public static boolean containsOrder(Order order) {
		return allOrders.contains(order);
	}

}
