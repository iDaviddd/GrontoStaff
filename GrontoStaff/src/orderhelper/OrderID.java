package orderhelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrderID {
	
	private static List<Integer> ids = new ArrayList<Integer>();
	private static final int RANGE = 100000;
	
	private static int index;
	
	static {
		
		for(int i = 0; i < RANGE; i++) {
			ids.add(i);
		}
		Collections.shuffle(ids);
	}
	
	private OrderID() {
		
	}
	
	public static int getID() {
		if(index > ids.size()) index = 0;
		return ids.get(index++);
	}

}
