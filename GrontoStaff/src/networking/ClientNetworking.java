package networking;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class ClientNetworking {
	
	private static final long serialVersionUID = 1L;

	private DatagramSocket socket;

	private String restaurant, address;
	private int port;
	private InetAddress inetAddress;
	private int ID = -1;

	private Thread send;

	public ClientNetworking(String restaurant, String address, int port) {
		this.restaurant = restaurant.trim();
		this.address = address;
		this.port = port;
	}

	/**
	 * Opens a connection to the server.
	 * 
	 * @return if the connection was successful or not.
	 */
	public boolean openConnection(String address) {
		try {
			socket = new DatagramSocket();
			inetAddress = InetAddress.getByName(address);
		} catch (UnknownHostException e) {
			return false;
		} catch (SocketException e) {
			return false;
		}
		return true;
	}

	/**
	 * Receive packets method.
	 * 
	 * @return String
	 */
	public String receiveData() {
		byte[] data = new byte[1024];
		DatagramPacket packet = new DatagramPacket(data, data.length);
		try {
			socket.receive(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String datamessage = new String(packet.getData());
		return datamessage;
	}

	/**
	 * Sends an array of bytes to the server.
	 */
	public void sendData(final byte[] data) {
		send = new Thread("Send") {
			public void run() {
				DatagramPacket packet = new DatagramPacket(data, data.length, inetAddress, port);
				try {
					socket.send(packet);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		send.start();
	}

	/**
	 * Closes the connection. 
	 */
	public void close() {
		new Thread() {
			public void run() {
				synchronized (socket) {
					socket.close();
				}
			}
		}.start();
	}
	
	public String getRestaurant() {
		return restaurant;
	}

	public String getAdress() {
		return address;
	}

	public int getPort() {
		return port;
	}

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

}
