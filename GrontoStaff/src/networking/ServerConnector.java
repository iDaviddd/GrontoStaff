package networking;

import java.net.DatagramSocket;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class ServerConnector implements Runnable {

	private DatagramSocket socket; // This is the actual connection to the server.

	private boolean running = false;

	private Thread listen, run; // The thread we're running send on.

	private ClientNetworking clientnetwork;

	public ServerConnector(String restaurant, String address, int port) {
		clientnetwork = new ClientNetworking(restaurant, address, port);
		System.out.println("Ansluter till servern: " + address + ":" + port + " som restaurang " + restaurant);
		boolean connect = clientnetwork.openConnection(address);
		if (!connect) {
			String txt = "Kunde inte ansluta till servern... F�rs�k en g�ng till, annars �r den sannolikt nere.";
			Alert alert = new Alert(AlertType.ERROR, txt, ButtonType.OK);
			alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			alert.showAndWait();
			System.exit(0);
		}
		String connection = "/d/" + restaurant + "/e/";
		clientnetwork.sendData(connection.getBytes());

		running = true;
		run = new Thread(this, "Running");
		run.start();
	}

	/**
	 * Starts the listen method.
	 */
	public void run() {
		listen();
	}

	/**
	 * Listens for incoming data from the server.
	 */
	public void listen() {
		listen = new Thread() {
			public void run() {
				while (running) {
					String data = clientnetwork.receiveData();
					if (data.startsWith("/d/")) {// Newly connected user receives ID from server.
						clientnetwork.setID(Integer.parseInt(data.split("/d/|/e/")[1]));
					} else if (data.startsWith("/p/")) { // Handle ping.
						String text = "/p/" + clientnetwork.getID() + "/e/";
						sendToServer(text);
					} else if (data.startsWith("/o/")) {
						handleOrder(data);
					} else {

					}
				}
			}
		};
		listen.start();
	}
	
	private void handleOrder(String data) {
		//TODO: Handle order. 
	}

	/**
	 * Sends a string to the server. 
	 * @param message String to be sent. 
	 */
	private void sendToServer(String message) {
		if (message.equalsIgnoreCase(""))
			return;
		clientnetwork.sendData(message.getBytes());
	}

}
