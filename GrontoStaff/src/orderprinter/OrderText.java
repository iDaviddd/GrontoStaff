package orderprinter;

import java.util.ArrayList;
import java.util.Iterator;

import orderhelper.Order;
import orderhelper.Product;

public class OrderText {
	private ArrayList<String> documentText;
	private Order order;

	/**
	 * Creates the text strings used in OrderDocument to form a document.
	 * 
	 * @param order
	 *            is the selected order
	 */
	public OrderText(Order order) {
		this.order = order;
		documentText = new ArrayList<String>();
	}

	/**
	 * Creates an iterator containing strings to add to the document
	 * which will be printed
	 * 
	 * @return Iterator<String> of information about the customer, the customer's
	 *         order and the products ordered.
	 */
	public Iterator<String> createText() {
		documentText.add(0, "KUNDINFORMATION"); // Information about the customer
		documentText.add(1, "Namn: " + order.getCustomer().getName());
		documentText.add(2, "Stad: " + order.getCustomer().getCity());
		documentText.add(3, "Postnummer: " + order.getCustomer().getZipCode());
		documentText.add(4, "Address: " + order.getCustomer().getAddress());
		documentText.add(5, "Telefonnummer: " + order.getCustomer().getCellPhone());
		documentText.add(6, "Emailaddress: " + order.getCustomer().getEmail());
		documentText.add(7, "");

		documentText.add(8, "ORDERINFORMATION"); // Information about the order;
		documentText.add(9, "OrderID: " + ((Integer) order.getOrderID()).toString());
		documentText.add(10, "Tid sedan ordern beställdes: " + ((Long) order.getElapsedHours()).toString() + "h och "
				+ ((Long) order.getElapsedMinutes()) + "min");
		documentText.add(11, "Summa Pris: " + order.getPrice() + "kr");
		documentText.add(12,"");
		
		documentText.add(13, "PRODUKTER BESTÄLLDA");
		int index = 14;
		Product[] products = order.getProducts();
		for (int i = 0; i < order.getProducts().length; i++) { // Information about the products ordered
			documentText.add(index, "Produkt: " + products[i].getName());
			index++;
			documentText.add(index, "Antal: " + ((Integer) products[i].getQuantity()).toString() + "st");
			index++;
			documentText.add(index, "Kundmeddelande: " + products[i].getComment());
			index++;
			documentText.add(index, "");
			index++;
		}

		
		documentText.add(index, "Summa Pris: " + order.getPrice() + "kr");

		return documentText.iterator();
	}
}
