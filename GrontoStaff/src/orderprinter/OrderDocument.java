package orderprinter;

import java.io.IOException;
import java.util.Iterator;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import orderhelper.Order;
import orderhelper.Product;

public class OrderDocument {
	private Order order;
	private int xpos;
	private int ypos ;
	private int nbrOfPages;
	private PDDocument document;
	private PDPage currentPage;
	/**
	 * Creates a PDF-document consisting of information from a
	 * selected order.
	 * 
	 * @param order
	 *            is the order which the document contains information about.
	 */
	public OrderDocument(Order order, PDDocument document) {
		this.order = order;
		nbrOfPages = 1;
		xpos = 0;
		ypos = 0;
		this.document = document;
	}
	/**
	 * Creates the document to be printed by instantiating OrderText and adding it's content to a document.
	 * @return a PDDocument containing information about the customer, the customer's
	 *         order and the products ordered.
	 * @throws IOException
	 */
	public PDDocument createDocument() throws IOException {
		PDFont font = PDType1Font.HELVETICA_BOLD;			//The font used in the document
		currentPage = new PDPage();
		document.addPage(currentPage);
		PDPageContentStream contentStream = new PDPageContentStream(document, currentPage,
				PDPageContentStream.AppendMode.APPEND, true);		//Append mode means added text to the document gets appended and doesn't overwrite previous text.
		Iterator<String> text = (new OrderText(order)).createText();
		
		
			contentStream.setFont( font, 24);
			contentStream.beginText();
			contentStream.newLineAtOffset( 155, 700);
			xpos += 155;
			ypos += 700;
			contentStream.showText("Utskrift av order");			//Title of document
			contentStream.setFont(PDType1Font.COURIER_OBLIQUE, 10);
			contentStream.moveTextPositionByAmount(200, 0);
			xpos += 200;
			contentStream.showText(" - powered by grabbarna boys");
			contentStream.newLineAtOffset( -265, -36);				
			xpos -= 265;
			ypos -= 36;
			
			contentStream.setFont( font, 12);						
			contentStream.setLeading(20);

			while(text.hasNext()) {
				contentStream.newLineAtOffset( 0, -18);
				ypos -= 18;
				contentStream.showText(text.next());
				contentStream = checkForNewPage(contentStream);
			}
			contentStream.newLineAtOffset(250, -18);
			contentStream.showText("Utskrift f�r OrderID " + order.getOrderID() + "  Sida: " + ((Integer) nbrOfPages).toString());
			contentStream.endText();
			contentStream.close();
	

			document.save("testpdf.pdf");
			document.close();
		return document;
		
	
	
	}
	
	/**
	 * Checks if the text position in y-axis is close the bottom of the page and if needed adds a new page to the document
	 * @param contentStream is the previous contentStream
	 * @return the same contentStream if the text position in y-axis is not at the bottom page otherwise a new contentStream with a new page.
	 * @throws IOException
	 */
	public PDPageContentStream checkForNewPage(PDPageContentStream contentStream) throws IOException {
		if (ypos <= 200) {
		contentStream.newLineAtOffset(250, -100);
		contentStream.showText("Utskrift f�r OrderID " + order.getOrderID() + "  Sida: " + ((Integer) nbrOfPages).toString());
		contentStream.endText();
		contentStream.close();
		currentPage = new PDPage();
		document.addPage(currentPage);
		contentStream = new PDPageContentStream(document, currentPage,
				PDPageContentStream.AppendMode.APPEND, true);
		contentStream.beginText();
		contentStream.newLineAtOffset(95, 700);
		ypos = 700;
		contentStream.setFont( PDType1Font.HELVETICA_BOLD, 12);						
		contentStream.setLeading(20);
		nbrOfPages ++;
		}
		return contentStream;
	}
}
