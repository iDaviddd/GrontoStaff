package orderprinter;

import java.awt.print.PrinterJob;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

import orderhelper.Order;

public class OrderPrint {
	private Order order;

	/**
	 * Prints a PDF-document created in OrderDocument.
	 * 
	 * @param order
	 *            is the order selected to print
	 * 
	 */
	public OrderPrint(Order order) {
		this.order = order;
	}

	/**
	 * Takes OrderPrint's order and writes its' information in a PDF file
	 * and prints it to a connected printer
	 * 
	 * @throws Exception
	 */
	public void print() throws Exception {
		System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider"); // this improves performance
																							// but does no longer print
																							// in colors.

		PDDocument document = new PDDocument();
		OrderDocument od = new OrderDocument(order, document);
		try {
			document = od.createDocument();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PrintService myPrintService = findPrintService("Gronto's printer's name");

		PrinterJob job = PrinterJob.getPrinterJob();
		job.setPageable(new PDFPageable(document));
		job.setPrintService(myPrintService);
		job.print();

	}

	//TODO: Get printers name and replace the parameters in the instantiation of printServices with the printers name.
	private PrintService findPrintService(String printerName) {
		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
		for (PrintService printService : printServices) {
			if (printService.getName().trim().equals(printerName)) {
				return printService;
			}
		}
		return null;
	}
}