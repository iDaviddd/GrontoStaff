package main;

import java.util.HashMap;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import networking.ServerConnector;
import order.OrderWindow;
import orderhelper.OrderHandler;

public class LoginEvent implements EventHandler<ActionEvent>{
	
	private Stage stage;
	private PasswordField pwfield;
	private TextField usrfield;
	private ComboBox<String> restaubox;
	
	private HashMap<String, String> pwdusr = new HashMap<String, String>();
	
	public LoginEvent(Stage stage, PasswordField pwfield, TextField usrfield, ComboBox<String> restaubox) {
		this.stage = stage;
		this.pwfield = pwfield;
		this.usrfield = usrfield;
		this.restaubox = restaubox;
		pwdusr.put("admin", "admin");
	}
	
	@Override
	public void handle(ActionEvent event) {
		if(pwfield.getText().equalsIgnoreCase("") || usrfield.getText().equalsIgnoreCase("") || restaubox.getValue() == null) {
			String txt = "Skriv anv�ndarnamn, l�senord och v�lj restaurang!";
			Alert alert = new Alert(AlertType.ERROR, txt, ButtonType.OK);
			alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			alert.showAndWait();
			return;
		}
		stage.close();
		
		if(pwdusr.get(usrfield.getText()) != null && pwdusr.get(usrfield.getText()).equals(pwfield.getText())) {
			//TODO: Add new logged in window and establish connection to server.
			ServerConnector oc = new ServerConnector(restaubox.getValue(), "192.168.1.108", 25555);
			OrderHandler.initialize();
		} else {
			String txt = "Finns inget anv�ndarnamn som matchar det l�senordet!";
			Alert alert = new Alert(AlertType.ERROR, txt, ButtonType.OK);
			alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			alert.show();
			return;
		}
	}

}
