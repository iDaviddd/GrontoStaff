package main;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;


public class Login extends Application {
	
	

	/**
	 * Adds buttons, textfields, etc. 
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		BorderPane root = new BorderPane();
		
		root.setTop(makeMenu());
		root.setCenter(makeLoginForm(primaryStage));

		
		Scene scene = new Scene(root, 500, 550);
		primaryStage.setTitle("Gr�nt och gott - av grabbarna");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	
	/**
	 * Creates a menu bar at the top of the screen. 
	 * @return a MenuBar.
	 */
	private MenuBar makeMenu() {
		MenuBar menuBar = new MenuBar();
		Menu archiveMenu = new Menu("Arkiv");
		MenuItem[] menuItems = new MenuItem[1];
		
		menuItems[0] = new MenuItem("Exit");
		
		menuItems[0].setOnAction((ActionEvent e) -> {
			System.exit(0);
		});
		
		archiveMenu.getItems().addAll(menuItems);
		
		Menu om = new Menu("Om");
		MenuItem cred = new MenuItem("Cred");
		
		cred.setOnAction((ActionEvent e) -> {
			String txt = "All �ra f�r utvecklingen av systemet g�r till David Albertsson, Anthony Bui, Aston �kerman, Damil Sabotic, Oscar Wiklund och Viktor Karlsson.";
			Alert alert = new Alert(AlertType.INFORMATION, txt, ButtonType.OK);
			alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			alert.show();
		});
		
		om.getItems().addAll(cred);
		
		menuBar.getMenus().add(archiveMenu);
		menuBar.getMenus().add(om);
		
		return menuBar;
	}
	
	/**
	 * Creates a login form made from a gridpane containing labels, textfields, a button and a combobox .
	 * @return the GridPane with login form.
	 */
	
	private GridPane makeLoginForm(Stage stage) {
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(20);
		grid.setVgap(10);
		Label usrlbl = new Label("Anv�ndarnamn: ");
		usrlbl.setFont(new Font("Abel", 24));
		TextField usrfield = new TextField();
		usrfield.setFont(Font.font("Abel", FontWeight.BOLD, 24));
		usrfield.setPrefHeight(70);
		usrfield.setPrefWidth(400);
		
		Label pwlbl = new Label("L�senord: ");
		pwlbl.setFont(new Font("Abel", 24));
		PasswordField pwfield = new PasswordField();
		pwfield.setFont(Font.font("Abel", FontWeight.BOLD, 24));
		pwfield.setPrefHeight(70);
		pwfield.setPrefWidth(400);
		
		ObservableList<String> restauList =
				FXCollections.observableArrayList(
						"Helsingkrona",
						"Malm�City",
						"LundCity"
				    );
		final ComboBox<String> restaubox = new ComboBox<String>(restauList);
		restaubox.setMinWidth(400);
		restaubox.setMinHeight(70);
		restaubox.setStyle("-fx-font: 24px \"Abel\";");
		
		Label restaulbl = new Label("Restaurang: ");
		restaulbl.setFont(new Font("Abel", 24));
		
		HBox buttonbox = new HBox();
		buttonbox.setAlignment(Pos.BOTTOM_LEFT);
		
		Button loginbtn = new Button("Logga in");
		loginbtn.setFont(new Font("Abel", 24));
		loginbtn.setStyle("-fx-background-color: #D0D0D0");
		loginbtn.setDefaultButton(true);
		loginbtn.setMinWidth(400);
		loginbtn.setMinHeight(70);
		loginbtn.setOnAction((EventHandler<ActionEvent>) new LoginEvent(stage, pwfield, usrfield, restaubox));
		
		buttonbox.getChildren().add(loginbtn);
		
		grid.add(usrlbl		, 0, 0);
		grid.add(usrfield	, 0, 1);
		grid.add(pwlbl		, 0, 2);
		grid.add(pwfield	, 0, 3);
		grid.add(restaulbl  , 0, 4);
		grid.add(restaubox, 0, 5);
		grid.add(buttonbox, 0, 7);
		
		return grid;
	}
	public static void main(String[] args) {
		Application.launch(args);
	}
}
