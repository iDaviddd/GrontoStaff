package order;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import orderhelper.Order;
import orderhelper.OrderComparator;
import orderhelper.OrderHandler;

public class OrderWindow {

	private ObservableList<Order> orders;
	private boolean isOpen;

	public OrderWindow() {
		isOpen = true;
		Stage stage = OrderHandler.orderstage;
		stage.hide();
		orders = FXCollections.observableArrayList();
		for (Order o : OrderHandler.allOrders) {
			orders.add(o);
		}

		ListView<Order> listView = new ListView<Order>(orders);

		listView.setCellFactory(new Callback<ListView<Order>, ListCell<Order>>() {
			@Override
			public ListCell<Order> call(ListView<Order> listView) {
				return new CustomListCell();
			}
		});

		BorderPane root = new BorderPane();
		root.setCenter(listView);

		listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Order>() {

			@Override
			public void changed(ObservableValue<? extends Order> observable, Order oldValue, Order selected) {
				isntOpen();
				new OrderInfo(selected);
			}

		});

		
		stage.setScene(new Scene(root, 400, 500));
		stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
		stage.setFullScreen(true);
		stage.show();
	}
	
	public boolean getIsOpen() {
		return isOpen;
	}
	
	public void isntOpen() {
		isOpen = false;
	}
	
	public void sortList() {
		orders.sort(new OrderComparator<Order>());
	}

	/**
	 * Adds an order to the UI.
	 * 
	 * @param order
	 *            order to be added.
	 */
	public void addOrder(Order order) {
		orders.add(order);
	}
	
	
	//TODO: Fix regions width so the "Time Remaining"  in the client window lines up.
	private class CustomListCell extends ListCell<Order> {
		private HBox content;
		private Text name;
		private Text price;
		private Text orderID;
		private Text timeLeft;
		private Region region;
		
		private HBox timebox;

		public CustomListCell() {
			/*
			 * This nested class creates and modifies the cells of the ListView.
			 */
			super();
			name = new Text();
			price = new Text();
			orderID = new Text();
			timeLeft = new Text();
			region = new Region();
			VBox vBox = new VBox(name, price);
			timebox = new HBox( region, timeLeft);
			

			content = new HBox(orderID, vBox, timebox);
			content.setPadding(new Insets(10));
			content.setSpacing(200);
			content.setPrefHeight(100);
		}

		@Override
		protected void updateItem(Order order, boolean empty) {
			super.updateItem(order, empty);
			if (order != null && !empty) { // Test for null order and empty parameter
				name.setText(order.getCustomer().getName());
				price.setText(String.format("%d kronor", order.getPrice()));
				orderID.setText(Integer.toString(order.getOrderID()));
				timeLeft.setText(order.getElapsedHours() + "h " + order.getElapsedMinutes() + "m");
				
				timeLeft.setFont(new Font("Abel", 24));
				name.setFont(new Font("Abel", 24));
				price.setFont(new Font("Abel", 24));
				orderID.setFont(new Font("Abel", 24));
				

				region.setPrefHeight(10);												//NEEDS BUG FIX
				region.minWidth(400 - timeLeft.getBoundsInLocal().getWidth());
				HBox.setHgrow(region, Priority.ALWAYS);
				
				switch (order.getStatus()) {
				case SEN:
					setStyle("-fx-background-color: #ff5050;");
					break;
				case V�NTAR:
					setStyle("-fx-background-color: #ffc864;");
					break;
				case KLAR:
					setStyle("-fx-background-color: #64c864;");
					break;
				case LEVERERAD:
					setStyle("-fx-background-color: #328232;");
					break;
				}
				setGraphic(content);
			} else {
				setGraphic(null);
			}
		}
	}

}
