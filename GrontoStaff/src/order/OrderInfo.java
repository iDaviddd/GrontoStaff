package order;

import java.awt.Toolkit;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Callback;
import orderhelper.Order;
import orderhelper.OrderHandler;
import orderhelper.OrderStatus;
import orderhelper.Product;
import orderprinter.OrderPrint;

public class OrderInfo {

	private Order order;
	private Label status;

	/**
	 * OrderInfo shows specific information to every order
	 * 
	 * @param oldScene
	 *            is the previous page
	 * @param stage
	 *            is the current stage
	 * @param selected
	 *            is the chosen order
	 */
	public OrderInfo(Order selected) {
		this.order = selected;
		System.out.println(selected);
		status = new Label("Status: " + order.getStatus().toString());
		status.setFont(new Font("Abel", 36));
		createPage(selected);
	}

	/**
	 * Creates a blank page with buttons to go back to the previous page or to exit
	 * the program
	 *
	 */
	private void createPage(Order order) {
		BorderPane borderpane = new BorderPane();

		HBox buttonbox = new HBox();
		buttonbox.setAlignment(Pos.CENTER);

		addButtons(buttonbox);// Adds the buttons to the buttonbox.

		borderpane.setTop(makeInfoBox(order));
		borderpane.setCenter(makeOrderListView(order));
		borderpane.setBottom(buttonbox);
		OrderHandler.orderstage.hide();
		OrderHandler.orderstage.setScene(new Scene(borderpane));
		OrderHandler.orderstage.setFullScreen(true);
		OrderHandler.orderstage.show();
	}

	// TODO: Remove color coding on the button but color code something on the page.

	/**
	 * changeStatusButton() adds a button which changes the status on the order when
	 * fired.
	 */
	private void addButtons(HBox box) {
		double buttonwidth = Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 3;
		Button statusbtn = new Button();
		String label1 = "KLAR";
		String label2 = "LEVERERAD";
		String label3 = "�TERST�LL";
		switch (order.getStatus()) {
		case SEN:
			statusbtn.setText(label1);
			statusbtn.setStyle("-fx-background-color: #64c864;");
			break;
		case V�NTAR:
			statusbtn.setText(label1);
			statusbtn.setStyle("-fx-background-color: #64c864;");
			break;
		case KLAR:
			statusbtn.setText(label2);
			statusbtn.setStyle("-fx-background-color: #328232;");
			break;
		case LEVERERAD:
			statusbtn.setText(label3);
			statusbtn.setStyle("-fx-background-color: #ffffff;");
			break;
		}

		statusbtn.setMinWidth(buttonwidth);
		statusbtn.setMinHeight(70);
		statusbtn.setFont(new Font("Abel", 24));

		statusbtn.setOnAction((event) -> {
			switch (order.getStatus()) {
			case SEN:
				order.setStatus(OrderStatus.KLAR);
				statusbtn.setText(label1);
				statusbtn.setStyle("-fx-background-color: #328232;");
				updateStatus();
				break;
			case V�NTAR:
				order.setStatus(OrderStatus.KLAR);
				statusbtn.setText(label2);
				statusbtn.setStyle("-fx-background-color: #328232;");
				updateStatus();
				break;
			case KLAR:
				order.setStatus(OrderStatus.LEVERERAD);
				statusbtn.setText(label3);
				statusbtn.setStyle("-fx-background-color: #ffffff;");
				updateStatus();
				break;
			case LEVERERAD:
				// TODO: Kolla tiden - t�nk om den var sen?
				order.setStatus(OrderStatus.V�NTAR);
				statusbtn.setText(label1);
				statusbtn.setStyle("-fx-background-color: #64c864;");
				updateStatus();
			}
		});

		Button backbtn = new Button("Tillbaka");
		backbtn.setDefaultButton(true);
		backbtn.setMinWidth(buttonwidth);
		backbtn.setMinHeight(70);

		backbtn.setOnAction((event) -> {
			OrderHandler.newOrderWindow();
		});

		backbtn.setFont(new Font("Abel", 24));
		backbtn.setStyle("-fx-background-color: #D0D0D0");

		Button printbtn = new Button("Skriv ut");
		printbtn.setDefaultButton(true);
		printbtn.setPrefHeight(70);
		printbtn.setPrefWidth(buttonwidth);
		printbtn.setFont(new Font("Abel", 24));
		printbtn.setStyle("-fx-background-color: #D0D0D0");

		printbtn.setOnAction((event) -> {
			OrderPrint orderPrint = new OrderPrint(order);
			try {
				printbtn.setDisable(true);
				printbtn.setText("Utskriven");
				printbtn.setStyle("-fx-background-color: #6d6d6d");
				orderPrint.print();
			} catch (Exception e) {
				System.out.print(e.toString() + e.getMessage());
			}
			;
		});

		box.getChildren().add(backbtn);
		box.getChildren().add(printbtn);
		box.getChildren().add(statusbtn);
	}

	private void updateStatus() {
		status.setText("Status: " + order.getStatus().toString());
	}

	private ListView<Product> makeOrderListView(Order o) {
		ObservableList<Product> products = FXCollections.observableArrayList();

		for (Product p : o.getProducts()) {
			products.add(p);
		}

		final ListView<Product> listView = new ListView<Product>(products);
		listView.setCellFactory(new Callback<ListView<Product>, ListCell<Product>>() {
			@Override
			public ListCell<Product> call(ListView<Product> listView) {
				return new CustomListCell();
			}
		});

		return listView;
	}

	/**
	 * Creates all the labels.
	 * 
	 * @param order
	 *            present order
	 * @return the VBox
	 */
	private VBox makeInfoBox(Order order) {
		VBox vbox = new VBox();
		HBox hbox1 = new HBox();
		hbox1.setSpacing(90);
		HBox hbox2 = new HBox();
		hbox2.setSpacing(90);
		HBox hbox3 = new HBox();
		hbox3.setSpacing(90);
		HBox hbox4 = new HBox();
		hbox4.setSpacing(90);
		final Font font = new Font("Abel", 36);

		Label name = new Label("Namn: " + order.getCustomer().getName());
		name.setFont(font);
		Label zipcode = new Label("Postkod: " + order.getCustomer().getZipCode());
		zipcode.setFont(font);
		Label city = new Label("Stad: " + order.getCustomer().getCity());
		city.setFont(font);
		Label address = new Label("Address: " + order.getCustomer().getAddress());
		address.setFont(font);
		Label email = new Label("Epost: " + order.getCustomer().getEmail());
		email.setFont(font);
		Label orderID = new Label("OrderID: " + Integer.toString(order.getOrderID()));
		orderID.setFont(font);
		Label time = new Label("Passerad tid: " + order.getElapsedHours() + "h och " + order.getElapsedMinutes() + "m");
		time.setFont(font);

		// temporary button to exit the program

		Button exitbtn = new Button("AVSLUTA");
		exitbtn.setPrefHeight(35);
		exitbtn.setPrefWidth(100);
		exitbtn.setFont(new Font("Abel", 18));
		exitbtn.setOnAction((event) -> {
			System.exit(0);
		});
		// status is already made

		hbox1.getChildren().add(name);
		hbox1.getChildren().add(zipcode);
		hbox1.getChildren().add(city);
		hbox2.getChildren().add(address);
		hbox2.getChildren().add(email);
		hbox3.getChildren().add(orderID);
		hbox3.getChildren().add(time);
		hbox4.getChildren().add(status);
		hbox4.getChildren().add(exitbtn);

		// TODO: Add information about order to hboxes.

		vbox.getChildren().add(hbox1);
		vbox.getChildren().add(hbox2);
		vbox.getChildren().add(hbox3);
		vbox.getChildren().add(hbox4);
		return vbox;
	}

	private class CustomListCell extends ListCell<Product> {
		private HBox content;
		private Text productname;
		private Text productquantity;
		private Text productcomment;

		public CustomListCell() {
			/*
			 * This nested class creates and modifies the cells of the ListView.
			 */
			super();
			productname = new Text();
			productquantity = new Text();
			productcomment = new Text();
			VBox vBox = new VBox(productname, productcomment);
			content = new HBox(vBox, productquantity);
			content.setSpacing(30);
			content.setPadding(new Insets(10));
			content.setPrefHeight(100);
		}

		@Override
		protected void updateItem(Product product, boolean empty) {
			super.updateItem(product, empty);
			if (product != null && !empty) { // Test for null order and empty parameter
				productname.setText(product.getName());
				productquantity.setText(String.format("x %d", product.getQuantity()));
				productcomment.setText(product.getComment());
				productname.setFont(new Font("Abel", 24));
				productquantity.setFont(new Font("Abel", 24));
				productcomment.setFont(new Font("Abel", 24));
				setGraphic(content);
			} else {
				setGraphic(null);
			}
		}
	}

}
